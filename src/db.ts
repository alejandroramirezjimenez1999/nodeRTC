import { DataSource } from "typeorm"
import { Person } from "./models/person.model"

export const AppDataSource = new DataSource({
    type: "mariadb",
    host: "localhost",
    port: 3306,
    username: "root",
    password: "spainusa2023",
    database: "cot",
    synchronize: true,
    logging: false,
    entities: [Person],
    migrations: [],
    subscribers: [],
})