import { Router } from 'express';
import { addPerson, getPersons } from '../controllers/person.controller';

const router = Router();

router.get('/persons', getPersons);
router.post('/persons', addPerson);

export default router;
