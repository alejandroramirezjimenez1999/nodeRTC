import { Request, Response } from 'express';
import { Person } from '../models/person.model';

export const getPersons = async (req: Request, res: Response): Promise<void> => {
  const persons = await Person.find();
  res.json(persons);
};

export const addPerson = async (req: Request, res: Response): Promise<void> => {
  const person = await Person.create(req.body);
  res.json(person);
};
