import cors from 'cors';
import express from 'express';
import http from 'http';
import "reflect-metadata";
import { AppDataSource } from './db';
import personRoutes from './routes/person.routes';
import { RTCClientHandler } from './rtcClient';


const app = express();
app.use(express.json());
app.use('/api', personRoutes);
const server = http.createServer(app);

// CORS
app.use(cors({
  origin: 'http://localhost:4200',
  methods: ['GET', 'POST'],
  credentials: true
}));



server.listen(3000,'0.0.0.0', () => {
  console.log('Servidor escuchando en el puerto 3000');
});

AppDataSource.initialize().then(() => {
  console.log('Connected!');
}).catch((error) => console.log(error))

//const rtcHandler:RTCServerHandler = new RTCServerHandler(server);

const rtcHandler:RTCClientHandler = new RTCClientHandler(server);






